﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;

namespace tcp_user
{
    class Program
    {
        static void Main(string[] args)
        {
            const string END = "`";
            bool endProgram = false;
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("configuration.json");
            IConfigurationRoot configuration = builder.Build();
            IPAddress ipAddress = IPAddress.Parse(configuration.GetSection("Network").GetSection("Address").Value);
            int port = Int32.Parse(configuration.GetSection("Network").GetSection("Port").Value);

            try {
                IPEndPoint remoteEP = new IPEndPoint(ipAddress,port);
                Socket socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                try {
                    socket.Connect(remoteEP);
                    Console.WriteLine("Socket connected to {0}", socket.RemoteEndPoint.ToString());

                    Thread listenThread = new Thread(() =>
                    {
                        Thread.CurrentThread.IsBackground = true;

                        byte[] bytes = new Byte[1024];
                        string received = null;

                        Console.WriteLine("Socket listening to {0}", socket.RemoteEndPoint.ToString());

                        while (!endProgram)
                        {
                            received = null;
                            while (true)
                            {
                                int bytesRec = socket.Receive(bytes);
                                received += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                                if (received.IndexOf(END) > -1)
                                {
                                    break;
                                }
                            }
                            Console.WriteLine("Received: {0}", received);
                        }

                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();
                    });

                    listenThread.Start();

                    while (!endProgram)
                    {
                        Console.WriteLine("Send message, or q to quit:");
                        string sent = Console.ReadLine();
                        if (sent == "q")
                        {
                            endProgram = true;
                            break;
                        }
                        byte[] msg = Encoding.ASCII.GetBytes(sent + END);

                        int bytesSend = socket.Send(msg);
                        Console.WriteLine("Sent: {0}", Encoding.ASCII.GetString(msg,0,bytesSend));
                    }

                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();

                } catch (ArgumentNullException ane) {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                } catch (SocketException se) {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                } catch (Exception e) {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }            
        }
    }
}
